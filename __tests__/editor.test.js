//Tests for Markdown Editor component

import { mount } from '@vue/test-utils'
import MarkdownEditor from './../src/components/MarkdownEditor.vue'

describe('Editor', () => {
    // Inspect the raw component options
    it('has data', () => {
      expect(typeof MarkdownEditor.data).toBe('function')
    })
})

describe('Mounted App', () => {
    const wrapper = mount(MarkdownEditor);  
    
    it('renders the correct markup', () => {
        //wrapper.setData({ input: '# Hello World' })
        expect(wrapper.html()).toContain('H1')
    })
})

