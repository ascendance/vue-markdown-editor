# markdown-editor

## Project setup
```
npm install
```

## Run tests
```
npm test
```

### Compiles and hot-reloads for development
```
npm start
```
### Compiles tailwind:css
```
npm run build:css
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
