//const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'media',
  //removes unused css during npm run build:css
  purge: [
     './src/**/*.html',
     './src/**/*.js',
     './src/**/*.vue',
  ],
  theme: {    
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
      serif: ['Courier', 'serif'],
    }
  },
  variants: {
    extend: {      
      borderColor: ['focus-visible'],
      opacity: ['disabled'],
    }
  },
  plugins: [
    require('@tailwindcss/typography'),    
  ],
}
